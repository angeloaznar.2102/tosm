Types standars: - int   -> entier numérique
                - float -> nombre flottant
                - str   -> chaine de caractère
                - bool  -> booléen
                - lst   -> liste

Fonctions : print() -> affiche a l'écran
            input() -> lire au clavier
            type()  -> retourne le type de données, variables, etc.
            int(), float(), str(), bool() -> "caster" une donnée
            str.format() -> formater une chaine